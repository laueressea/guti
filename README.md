# Guti's Webpage

## Before making CSS changes
```
npm install -g sass
npm run sass
```
Check file `package.json` to see what `npm run sass` does (See "scripts").

## Pending
- Navigation animation. See [reference](https://codepen.io/karencho/pen/KIgur).
- Contact form. Fields: Name (text input), Email (email input), Message (textarea).
- Validate form with HTML5 ([docs](https://www.html5rocks.com/en/tutorials/forms/constraintvalidation/)). All fields required.
- Send via AJAX on submit (check with Aure).

## Helpful docs
- [Git tutorial](http://rogerdudler.github.io/git-guide/index.es.html). Not complete, just basics.
