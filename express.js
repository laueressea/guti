var express = require('express');
var bodyParser = require('body-parser')
var app = express();

app.use(express.static('.'));
app.use(bodyParser.urlencoded({ extended: false }));

// respond with "hello world" when a GET request is made to the homepage
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.post('/contact', (req, res) => {
  console.log(req.body);
  res.send('success');
});

app.listen(8888, () => console.log('Example app listening on port 8888!'))
