<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>Gutiérrez Group</title>
  </head>
  <body>
    <div class="body-bg">

        <!-- Barra de navegación superior horizontal fija -->

        <div class="navibar navibar--hidden">
            <div class="navibar__top-container">
                <a class="scroll-btn navibar__icon" style="background-image: url(images/Gicon2.jpg)" href="#t1"></a>
                <div class="navibar__toggle"><i class="fas fa-bars"></i></div>  
            </div>
            <ul class="navibar__btns-container">
                <li class="navibar__btns-container__btn"><a class="scroll-btn" href="#t1">Inicio</a></li>
                <li class="navibar__btns-container__btn"><a class="scroll-btn" href="#t2">Acerca de</a></li>
                <li class="navibar__btns-container__btn"><a class="scroll-btn" href="#t3">Miembros</a></li>
                <li class="navibar__btns-container__btn"><a class="scroll-btn" href="#t4">Galería</a></li>
                <li class="navibar__btns-container__btn"><a class="scroll-btn" href="#t5">Contacto</a></li>
            </ul>
                        
        </div>

       <!-- PRIMERA SECCIÓN "HEADER" -->

        <header id="t1" class="guti-header" style="background-image: url(images/bgimage.jpg)">
            <div class="guti-header__title-container">
                <h1 class="guti-header__title">¡Hola Guti!</h1>
                <h2 class="guti-header__title">Ésta es la página de Gutiérrez</h2>
            </div>
            <div class="arrow-down bounce">
                <a class="scroll-btn" href="#t2"><img src="./images/arrow-down.png" alt="Ícono de flecha para abajo"></a>
            </div>
        </header>

        <!-- SEGUNDA SECCIÓN "ACERCA DE" -->

        <section class="about-us" id="t2">
            <h2>DE GUTIÉRREZ Y OTRAS HIERBAS</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris pulvinar, odio quis gravida hendrerit, neque odio mattis turpis, nec lobortis leo ipsum in urna. Aliquam scelerisque, leo vitae placerat porta, tellus ex egestas ligula, eget laoreet velit mauris sed erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer sodales malesuada nibh at iaculis. Etiam sed finibus metus. Aliquam et elementum leo. Ut vel arcu egestas, sagittis lorem ac, porttitor neque. Donec mollis odio eu velit aliquam, quis pharetra erat malesuada.

            Sed sit amet justo nec erat luctus placerat et eget nisl. Vivamus cursus et erat ut vestibulum. Ut condimentum dui ligula, sit amet euismod mi imperdiet in. Curabitur finibus auctor sem sed elementum. Integer varius a arcu ac feugiat. Integer commodo massa vitae dui ornare maximus. Nam ultrices venenatis nibh a laoreet. Aenean aliquet nulla quam, vel malesuada neque condimentum vitae. Nulla faucibus justo a odio convallis, non egestas orci ullamcorper. Morbi tempus felis ut dignissim rhoncus. Nullam luctus finibus convallis.
            
            Quisque aliquet porttitor lacinia. Fusce convallis, quam id ultricies varius, risus orci eleifend elit, et luctus neque lectus non lectus. Curabitur rhoncus a ligula ac maximus. Nam imperdiet, neque vitae sodales iaculis, orci arcu finibus nibh, id maximus erat lacus et nisi. Maecenas aliquet ullamcorper finibus. Nunc id feugiat lorem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In non accumsan lorem. Vivamus pellentesque felis rhoncus, efficitur tortor sit amet, scelerisque metus. Fusce lorem orci, semper ut laoreet non, tempor id est. Sed dictum consequat sem vel maximus. Sed vel congue metus, vel posuere magna.
            
            Curabitur semper consequat nisi in accumsan. Vestibulum sit amet enim nec erat convallis laoreet a tincidunt turpis. Aliquam vel bibendum metus. Sed at diam et dui hendrerit sodales. Nunc varius commodo nulla sit amet faucibus. Ut turpis dolor, mollis vel massa vel, sodales porttitor lacus. Vivamus risus nunc, porta vel ante vel, volutpat iaculis ante. Nunc vel risus quis elit rutrum tristique ac at lorem. Suspendisse et mollis elit. Morbi bibendum nisi eget libero finibus tempor. Praesent euismod sit amet augue at fermentum. Fusce eu augue eu augue tincidunt convallis. Vivamus venenatis aliquet lacus. Duis iaculis mi iaculis pulvinar iaculis. Vivamus id maximus nisi, vitae rhoncus augue. Nulla urna nunc, eleifend quis tortor eu, lacinia molestie tortor.
            
            Vestibulum faucibus tellus egestas, luctus tellus auctor, imperdiet tortor. Aliquam ac scelerisque ante. Nam placerat eleifend tortor a laoreet. Nunc tincidunt cursus felis ut posuere. Vivamus vestibulum non dolor sed pharetra. Phasellus ut lectus ac justo lobortis finibus eu vitae est. Vestibulum porta nec quam quis scelerisque. Donec et velit dolor. In ac lectus convallis, lobortis urna ut, tempor lectus. Donec nec tempor urna.</p>
            <div class="arrow-down bounce">
                <a class="scroll-btn" href="#t3"><img src="./images/arrow-down-black.png" alt="Ícono de flecha para abajo"></a>
            </div>
        </section>

        <!-- TERCERA SECCIÓN "MIEMBROS" -->

        <section class="members" id="t3">
            <h2 class="members__title">MIEMBROS</h2>
            <div class="members__portraits-container">
                <div class="members__portraits-container__group">
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(images/Ditto.png)"></div>
                        <p class="members__portrait__name">Aure</p>
                    </div>
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Santi</p>
                    </div>
                </div>
                <div class="members__portraits-container__group">
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Luca</p>
                    </div>
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Sir Martin</p>
                    </div>
                </div>
                <div class="members__portraits-container__group">
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Lau</p>
                    </div>
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Dani</p>
                    </div>
                </div>
                <div class="members__portraits-container__group">
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Belencin</p>
                    </div>
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Danu</p>
                    </div>
                </div>
                <div class="members__portraits-container__group">
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Osiris</p>
                    </div>
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Nicki</p>
                    </div>
                </div>
                <div class="members__portraits-container__group">
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Amigo Santi</p>
                    </div>
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Juli</p>
                    </div>
                </div>
                <div class="members__portraits-container__group">
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(./images/Ditto.png)"></div>
                        <p class="members__portrait__name">Giorgio</p>
                    </div>
                    <div class="members__portrait">
                        <div class="members__portrait__img" style="background-image: url(images/Ditto.png)"></div>
                        <p class="members__portrait__name">Fer</p>
                    </div>
                </div>
            </div>
            <div class="arrow-down bounce">
                <a class="scroll-btn" href="#t4"><img src="./images/arrow-down-black.png" alt="Ícono de flecha para abajo"></a>
            </div>
        </section>

        <!-- CUARTA SECCIÓN "GALERÍA" -->
        
        <section class="gallery" id="t4">
            <div class="gallery__content">
                <h2 class="gallery__title">GALERÍA DE IMÁGENES</h2>
                <div class="gallery__slider">
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_01.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_02.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_03.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_04.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_05.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_06.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_07.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_08.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_09.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_10.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_11.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_12.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_13.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_14.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_15.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_16.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_17.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_18.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_19.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_20.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_21.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_22.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_23.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_24.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_25.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_26.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_27.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_28.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_29.jpg)"></div>
                    </div>
                    <div class="gallery__slide-container">
                        <div class="gallery__slide__image" style="background-image: url(./images/Slide_30.jpg)"></div>
                    </div>
                </div>
            </div>    
        </section>

        <!-- QUINTA SECCIÓN "CONTACTO" -->

        <section class="contact-us" id="t5">
            <h2 class="contact-us__form-title">CONTACTANOS:</h2>
            <form class="contact-us__form-area ajax-contact" action="/contact" method="POST">
                <div class="contact-us__form-area__left">
                    <label class="contact-us__form-area__label" for="message">Mensaje:</label>
                    <textarea class="contact-us__form-area__text form-message" id="message" required></textarea>
                </div>
                <div class="contact-us__form-area__right">
                    <div class="contact-us__form-area__form-group">
                        <label class="contact-us__form-area__label" for="name">Name:</label>
                        <input type="text" class="form-control form-input" id="name" required>
                    </div>
                    <div class="contact-us__form-area__form-group form-email">
                        <label class="contact-us__form-area__label" for="email">Email address:</label>
                        <input type="email" class="form-control form-input" id="email" required>
                    </div>
                    <button type="submit" class="btn btn-primary enviar">Enviar</button>
                </div>
            </form>
        </section>

        <!-- FOOTER -->

        <footer>
            <p>-- Ningún derecho reservado --</p>
        </footer>

    </div>
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="slick/slick.min.js"></script>
    <script src="js/guticode.js"></script>
    </body>
  </html>
