//import 'bootstrap';
//SLIDER
$(".gallery__slider").slick({
    dots: false,
    autoplay: true,
    infinite: true,
    speed: 1000,
    fade: true,
    autoplaySpeed: 2000,
    lazyLoad: 'ondemand',
    cssEase: 'linear'
});

//NAVBAR STATE

$(window).scroll(function() {

    var topPos = $(this).scrollTop();

    $(".navibar").toggleClass("navibar--hidden", topPos < 150);

  });

$(".scroll-btn").on('click', function(event){
  event.preventDefault();
  var hash = this.hash;
  $('html, body').animate({
    scrollTop: $(hash).offset().top-$(".navibar").outerHeight()
  }, 800, function(){
  });
});

//NAVBAR TOGGLE MENU

$(".navibar__toggle").on('click', function(event){
	$(".navibar__btns-container").toggleClass("navibar__btns-container--active");
	$(".navibar__toggle").toggleClass("navibar__toggle--clicked");
})

//FORM

$(function() {

	// Get the form.
	var form = $('.ajax-contact');

	// Get the messages div.
	var formMessages = $('.form-message');

	// Set up an event listener for the contact form.
	form.on('submit',function(e) {
		// Stop the browser from submitting the form.
    e.preventDefault();

		// Serialize the form data.
		var formData = form.serialize();
    $(".contact-us__form-area").html('<div class="form-response"><p class="response-text">Cargando, esperá sentado.</p></div>');
		// Submit the form using AJAX.
		$.ajax({
			type: 'POST',
			url: $(form).attr('action'),
			data: formData
    })
		.done(function(response) {
			// Make sure that the formMessages div has the 'success' class.
			$(formMessages).removeClass('error');
			$(formMessages).addClass('success');

			// Set the message text.
			$(formMessages).text(response);
			/*// Clear the form.
			$('#name').val('');
			$('#email').val('');
      $('#message').val('');
      */
      //Change form-area
      var answer = response;
      $(".contact-us__form-area").html('<div class="form-response"><p class="response-text">Tu mensaje ha sido enviado con éxito! Besis ❤</p></div>');
		})
		.fail(function(data) {
			// Make sure that the formMessages div has the 'error' class.
			$(formMessages).removeClass('success');
			$(formMessages).addClass('error');
      // Set the message text.
			
			$(".contact-us__form-area").html('<div class="form-response"><p class="response-text">Mildis! Ocurrió un error y tu mensaje no pudo ser enviado 😶.</p></div>');
			
		});

	});

});